//RoboPedalFX

#define BLYNK_TEMPLATE_ID "TMPLA8JKKm54"
#define BLYNK_DEVICE_NAME "RoboPedalFX"
#define BLYNK_AUTH_TOKEN            "SaoCq5qk_adJtg6xOVucvW5mcMHOfhW2"


// Comment this out to disable prints and save space
#define BLYNK_PRINT Serial


#include <ESP8266WiFi.h>
#include <BlynkSimpleEsp8266.h>
#include <Servo.h>
#include <EEPROM.h>
char auth[] = BLYNK_AUTH_TOKEN;

// Your WiFi credentials.
// Set password to "" for open networks.
char ssid[] = "INFINITUM30A3_2.4";
char pass[] = "4xBkF93eVJ";

//DECLARACION DE LOS SERVOS//
Servo knob1;
Servo knob2;
Servo knob3;
Servo knob4;
Servo knob5;
Servo knob6;
Servo knob7;
Servo knob8;
Servo knob9;

//DECLARACION DE LOS PINES DE SERVOS//
int addr = 0;
int servo_pin1 = 16;
int servo_pin2 = 5;
int servo_pin3 = 4;
int servo_pin4 = 0;
int servo_pin5 = 2;
int servo_pin6 = 14;
int servo_pin7 = 12;
int servo_pin8 = 13;
int servo_pin9 = 15;

//DECLARACION DE MATRIZ DE MEMORIA//
#define rows (9)
#define cols (255)
int pos[rows][cols];

//BOTONES//
int Button_UP = LOW;
int Button_DOWN = LOW;
int Button_SET = LOW;
int Button_SAVE = LOW;
int PresetNumber = 1;
int aux = 0;

void setup()
{
  
  // Debug console
  Serial.begin(115200);
  EEPROM.begin(2304);
  Blynk.begin(auth, ssid, pass);
  // You can also specify server:
  //Blynk.begin(auth, ssid, pass, "blynk.cloud", 80);
  //Blynk.begin(auth, ssid, pass, IPAddress(192,168,1,100), 8080);

  knob1.attach(servo_pin1);
  knob2.attach(servo_pin2);
  knob3.attach(servo_pin3);
  knob4.attach(servo_pin4);
  knob5.attach(servo_pin5);
  knob6.attach(servo_pin6);
  knob7.attach(servo_pin7);
  knob8.attach(servo_pin8);
  knob9.attach(servo_pin9);
}

void loop()
{
  Blynk.run();
  // You can inject your own code or combine it with other sketches.
  // Check other examples on how to communicate with Blynk. Remember
  // to avoid delay() function!
  knob1.write(pos[0][0]);
  knob2.write(pos[1][0]);
  knob3.write(pos[2][0]);
  knob4.write(pos[3][0]);
  knob5.write(pos[4][0]);
  knob6.write(pos[5][0]);
  knob7.write(pos[6][0]);
  knob8.write(pos[7][0]);
  knob9.write(pos[8][0]);
  Blynk.virtualWrite(V24, PresetNumber);
}

BLYNK_WRITE(V0)
{
  pos[0][0] = param.asInt(); 
}

BLYNK_WRITE(V1)
{
  pos[1][0] = param.asInt(); 
}

BLYNK_WRITE(V2)
{
  pos[2][0] = param.asInt(); 
}

BLYNK_WRITE(V3)
{
  pos[3][0] = param.asInt(); 
}

BLYNK_WRITE(V4)
{
  pos[4][0] = param.asInt(); 
}

BLYNK_WRITE(V5)
{
  pos[5][0] = param.asInt(); 
}

BLYNK_WRITE(V6)
{
  pos[6][0] = param.asInt(); 
}

BLYNK_WRITE(V7)
{
  pos[7][0] = param.asInt(); 
}

BLYNK_WRITE(V8)
{
  pos[8][0] = param.asInt(); 
}

BLYNK_WRITE(V20)
{
 Button_UP = param.asInt();
 if (Button_UP == HIGH){
  if (PresetNumber < 255){
    PresetNumber++;
  } 
  else {
    PresetNumber = 1;  
  }
  }   
}

BLYNK_WRITE(V21)
{
 Button_DOWN = param.asInt();
 if (Button_DOWN == HIGH){
  if (PresetNumber > 1){
    PresetNumber--;
  } 
  else {
    PresetNumber = 255;  
  }
  }   
}

BLYNK_WRITE(V22)
{
  Button_SET = param.asInt();
  if (Button_SET == HIGH){
    
    for(int i=0; i<rows; i++){
    pos[i][0] = pos[i][PresetNumber];
    }
    Blynk.virtualWrite(V0, pos[0][0]);
    Blynk.virtualWrite(V1, pos[1][0]);
    Blynk.virtualWrite(V2, pos[2][0]);
    Blynk.virtualWrite(V3, pos[3][0]);
    Blynk.virtualWrite(V4, pos[4][0]);
    Blynk.virtualWrite(V5, pos[5][0]);
    Blynk.virtualWrite(V6, pos[6][0]);
    Blynk.virtualWrite(V7, pos[7][0]);
    Blynk.virtualWrite(V8, pos[8][0]);
  }
}

BLYNK_WRITE(V23)
{
  Button_SAVE = param.asInt();
  if (Button_SAVE == HIGH)
  {
    for(int i=0; i<rows; i++)
    {
      pos[i][PresetNumber] = pos[i][0];
      EEPROM.write(i,PresetNumber);
      EEPROM.end();
      Serial.println(pos[i][0]);
    }    
  }
}
