# RoboAMP - Open Source


## Si no has visto el video, ve a verlo! - Haz click en la imagen

[![RoboAMP en YouTube](Imágenes/Video%20YT.jpg)](https://www.youtube.com/watch?v=TlcM9L2CzjM&ab_channel=FuntronicsbyTechmake)

Este es un sistema robotico que nos permite automatizar y brindar las mejores cualidades de los sistemas digitales a los equipos de musica analogicos.

Crea tu propia version del RoboAMP, o diseña tus propios inventos utilizando el poder de la robotica y el IoT.

¡Gracias por colaborar!
